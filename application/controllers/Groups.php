<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends CI_Controller {

	public function index($database = 'default', $courseid = 1)
	{
		$this->load->database($database, FALSE);
		$this->load->model("GroupsModel");
        
        $data['Groups'] = $this->GroupsModel->getGroups($courseid);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}

	
	public function members($database = 'default', $groupid = 1){
		$this->load->database($database, FALSE);
		$this->load->model("GroupsMembersModel");
        
        $data['Members'] = $this->GroupsMembersModel->getGroupMembers($groupid);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}

}
