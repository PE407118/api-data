<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {

    public function index(){
        
    }

	public function forums($database = 'default',$course){
		$this->load->database($database, FALSE);
		$this->load->model("ForumModel");
        
        $data['existingForums'] = $this->ForumModel->existingForums($course);
        $this->output

        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function forumDisscuson($database = 'default',$forum = 1){
		$this->load->database($database, FALSE);
		$this->load->model("ForumModel");
        
        $data['forumDisscuson'] = $this->ForumModel->forumDisscuson($forum);
        $this->output

        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}

	public function forumPost($database = 'default',$discussion = 1){
		$this->load->database($database, FALSE);
		$this->load->model("ForumModel");
        
        $data['forumPost'] = $this->ForumModel->forumPost($discussion);
        $this->output

        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}