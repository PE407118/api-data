<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menssages extends CI_Controller {

    public function index()
    {
	}

	public function messagesRead($database = 'default',$useridfrom = 1){
		$this->load->database($database, FALSE);
		$this->load->model("MenssagesModel");
        
        $data['messagesRead'] = $this->MenssagesModel->messagesRead($useridfrom);
        $this->output

        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}

	
	public function unreadMessages($database = 'default', $useridfrom = 1){
		$this->load->database($database, FALSE);
		$this->load->model("MenssagesModel");
        
        $data['unreadMessages'] = $this->MenssagesModel->unreadMessages($useridfrom);
        $this->output

        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}
}