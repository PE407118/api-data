<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends CI_Controller {

	public function index($database = 'default')
	{
		$this->load->database($database, FALSE);
		$this->load->model("CoursesModel");
		
        $data['Courses'] = $this->CoursesModel->getCourses();
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}

}
