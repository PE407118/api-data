<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chats extends CI_Controller {

    public function index(){
        
    }

	public function courseChats($database = 'default',$course){
		$this->load->database($database, FALSE);
		$this->load->model("ChatsModel");
        
        $data['courseChats'] = $this->ChatsModel->courseChats($course);
        $this->output

        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function messagesChat($database = 'default',$groupid = 1){
		$this->load->database($database, FALSE);
		$this->load->model("ChatsModel");
        
        $data['messagesChat'] = $this->ChatsModel->messagesChat($groupid);
        $this->output

        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}
}