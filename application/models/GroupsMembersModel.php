<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GroupsMembersModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }
    

    public function getGroupMembers($groupid)
	{
        $this->db->select("id,groupid,userid");
        $this->db->where('groupid', $groupid);
		return $this->db->get("groups_members")->result();
	}
}