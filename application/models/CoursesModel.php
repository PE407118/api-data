<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CoursesModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }
    

    public function getCourses()
	{
        $this->db->select("id, fullname, shortname, timecreated");
		return $this->db->get("course")->result();
	}
}
