<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ForumModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }
    
    public function existingForums($course){
        $this->db->select("id,course,type,name,intro");
        $this->db->where('course', $course);
		  return $this->db->get("forum")->result();
    }

    public function forumDisscuson($forum){
      $this->db->select("id,course,forum,name");
      $this->db->where('forum', $forum);
    return $this->db->get("forum_discussions")->result();
    }

  public function forumPost($discussion){
    $this->db->select("id,discussion,parent,subject,message");
    $this->db->where('discussion', $discussion);
  return $this->db->get("forum_posts")->result();
  }
}