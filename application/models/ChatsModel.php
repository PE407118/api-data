<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ChatsModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }
    
    public function courseChats($course){
        $this->db->select("id,course,name");
        $this->db->where('course', $course);
		return $this->db->get("chat")->result();
    }

    public function messagesChat($groupid){
        $this->db->select("id,userid,groupid,message");
        $this->db->where('groupid', $groupid);
        return $this->db->get("chat_messages")->result();
    }
}