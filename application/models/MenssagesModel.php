<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenssagesModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }
    
    public function messagesRead($useridfrom){
        $this->db->select("id,useridfrom,useridto,subject,smallmessage");
        $this->db->where('useridfrom', $useridfrom);
        $this->db->or_where('useridto', $useridfrom);
		return $this->db->get("message_read")->result();
    }
    
    public function unreadMessages($useridfrom){
        $this->db->select('id,useridfrom,useridto,subject,smallmessage');
        $this->db->where('useridfrom', $useridfrom);
        $this->db->or_where('useridto', $useridfrom);
		return $this->db->get("message")->result();
	}
}