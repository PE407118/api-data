# ![](./public/images/api-data.png "Minning API") Minning API

API REST para la extracción de la información relacionada con la interacción asesor-alumno dentro de la plataforma.

## Endpoints

### Databases

Muestra un arreglo de las diferentes bases de datos que se pueden consultar.

[api-data/databases](http://localhost/minning/api-data/databases)

### Courses

Muestra un listado de los cursos que se encuentran en la base de datos seleccionada.

[api-data/courses/index/[database]](http://localhost/minning/api-data/courses/index/)

### Groups

#### Groups list

Cada curso cuenta con grupos que fueron definidos por el adminitrador.

[api-data/groups/index/[database]/[courseid]](http://localhost/minning/api-data/groups/index/)

#### Group members

Usuarios inscritos en el grupo seleccionado.

[api-data/groups/members/[database]/[groupid]](http://localhost/minning/api-data/groups/members/)

### Chats

#### Chat activities

Actividades tipo chat dentro del curso seleccionado.

[api-data/chats/index/[database]/[courseid]](http://localhost/minning/api-data/chats/index/)

#### Chat messages

Conversación de la actividad y grupo seleccionado.

[api-data/chats/messages/[database]/[chatid]/[groupid]](http://localhost/minning/api-data/chats/messages/)

### Forums

#### Forum activities

Actividades tipo foro dentro del curso seleccionado.

[api-data/forums/index/[database]/[courseid]](http://localhost/minning/api-data/forums/index/)

#### Forum discussions

Lista de _discusiones_ contenidas en el foro seleccionado.

[api-data/formus/discussions/[database]/[forumid]](http://localhost/minning/api-data/formus/discussions/)

#### Forum posts

Publicaciones realizadas dentro de la discusión seleccionada. Las publicaciones se muestran filtradas por grupo.

[api-data/formus/posts/[database]/[discussionid]/[groupid]](http://localhost/minning/api-data/formus/posts/)

### Messages

Listado de mensajes que tuvo determinado usuario con los demás usuarios. En este caso no existe un filtrado por grupos.

[api-data/messages/index/[database]/[userid]](http://localhost/minning/api-data/messages/index/)
____
## Notas adicionales

![](./public/images/postman.png "Postman") Para el desarrollo de los _Endpoints_ se recomienda el uso de la herramienta [Postman](https://www.getpostman.com/).

![](./public/images/danger.png "Danger") Favor de __excluir__ los siguientes archivos de los `commits`:
- [/application/config/config.php](./application/config/config.php)
- [/application/config/database.php](./application/config/database.php)